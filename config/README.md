mcsvc-infra-config-service
==========================

Spring Boot Cloud Config Server Implementation



Reference documentation
-----------------------
[Spring Cloud Config](https://spring.io/projects/spring-cloud-config)

[Spring Cloud Config Server Section](https://cloud.spring.io/spring-cloud-config/spring-cloud-config.html#_spring_cloud_config_server)

