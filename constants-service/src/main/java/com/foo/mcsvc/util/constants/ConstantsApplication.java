package com.foo.mcsvc.util.constants;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.core.env.Environment;

@Slf4j
@AllArgsConstructor
@EnableDiscoveryClient
@SpringBootApplication
public class ConstantsApplication {

    private Environment environment;


    public static void main(String[] args) {
        SpringApplication.run(ConstantsApplication.class, args);
    }

}
