package com.foo.mcsvc.util.constants;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "constants")
@Data
public class ConstantsConfiguration {
    private String firstName;
    private String lastName;
}
