package com.foo.mcsvc.util.constants;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/con",
        produces = "application/json")
public class ConstantsRestService {

    private final ConstantsConfiguration configuration;

    public ConstantsRestService(ConstantsConfiguration configuration) {
        this.configuration = configuration;
    }

    @GetMapping
    public ResponseEntity<ConstantsConfiguration> geConfiguration() {
        if (configuration != null)
            return ResponseEntity.ok(configuration);
        else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

}
