mcsvc-infra-eureka-service
==============================

Spring Boot cloud Netflix Eureka Implementation



Reference documentation
-----------------------
[Spring Cloud NetFlix](https://cloud.spring.io/spring-cloud-netflix/spring-cloud-netflix.html)

[Service Discovery Eureka Server](https://cloud.spring.io/spring-cloud-netflix/spring-cloud-netflix.html#spring-cloud-eureka-server)

