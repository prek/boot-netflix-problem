Spring Cloud Config Client Discovery issue
=========================================
[Stack Overflow Question](https://stackoverflow.com/questions/58269048/docker-spring-cloud-config-client-issue-with-config-server-discovery)

Run
---
mvn clean install
docker-compose up -d

[Eureka console](http://localhost:8761/)
[Config Server - retrieve config for constants-service](http://localhost:8888/constants-service/default)
[constants-service swagger page](http://localhost:8080/swagger-ui.htm)

Reproduce the issue
-------------------
On constants-service bootstrap.properties file
1. comment line 2
2. uncomment lines 4,5


Solution
--------

Checkout solution branch
